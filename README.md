tmux
====

Tmux permet: crear, accedir i controlar diversos terminals des d'una sola pantalla.
Permet desconnectar-se, executar en segon pla i tornar-se a connectar.

Una sessió té finestres enllaçades. Cada finestra ocupa tota la pantalla on aquesta es pot dividir en `panes`.

Tota sessió és persistent i sobreviurà a la desconnexió accidental (temps d'espera del ssh) o separació intencional `C-b d` i tornar-se a connectar amb `tmux attach-session`.

Mostra una sessió per la pantalla i totes les sessions són gestionades per un únic servidor.
